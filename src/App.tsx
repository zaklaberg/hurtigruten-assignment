import React from 'react';
import ShipSearch from './components/shipSearch/shipSearch'

function App() {
  return (
    <div className="App">
        <ShipSearch />
    </div>
  );
}

export default App;
