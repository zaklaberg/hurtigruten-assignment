import React, { useState, useEffect } from 'react'
import SearchField from '../shared/searchField/searchField'
import styles from './shipSearch.module.css'

const SERVER_URL = 'http://localhost:4000/'
const SHIPS_ENDPOINT = 'api/ships/'

// If enabled, grabs all ships in one go and stores them locally for future searches.
const ENABLE_CACHE = false

const ShipSearch = () => {
    const [results, setResults] = useState([])
    const [errorMessage, setErrorMessage] = useState('')
    const [cachedShips, setCachedShips] = useState([])

    // Triggers a search after user has stopped typing for this amount of time. 
    // Only relevant if caching is NOT enabled.
    // In milliseconds.
    const searchDelayTime = 700

    /**
     * Searches for any ship in the fleet that have a name including the given search term.
     * @param searchText: Text to search for in ship names
     * @throws on server error. User expected to catch.
     */
    const fetchShips = async searchText => {
        if (ENABLE_CACHE && cachedShips.length) {
            return cachedShips.filter(shipInfo => shipInfo.name.toLowerCase().includes(searchText.toLowerCase()))
        }

        const resp = await (await fetch(`${SERVER_URL}${SHIPS_ENDPOINT}${searchText}`)).json()
        if (ENABLE_CACHE) setCachedShips([...resp])
        return resp
        
    }

    // Fetch ships & display
    const onSearch = async searchText => {
        try {
            const ships = await fetchShips(searchText)
            setResults(ships)
            if (ships.length === 0) throw new Error('No ships matching your search could be found.')

            // Everything went well, clear any previous error messages
            setErrorMessage('')
        }
        catch(e) {
            setErrorMessage(e.message)
        }
    }

    const onSearchClear = () => {
        setResults([])
        setErrorMessage('')
    }

    // If caching, grab all the ships before user starts interacting.
    useEffect(() => {
        if (!ENABLE_CACHE) return
        
        fetchShips('')
        .catch(() => setErrorMessage('Server could not be reached.'))
    }, [])

    return (
        <div id={styles.theGreatShipSearch}>
            <span>
                <SearchField 
                    label="Search for one of our ships"
                    onSearch={onSearch}
                    onSearchClear={onSearchClear}
                    searchDelay={ENABLE_CACHE ? 0 : searchDelayTime}
                />
                <ul>
                    {
                        results.length > 0 && 
                        results.map((shipInfo, idx) => <li key={idx}>{shipInfo.name}</li>)
                    }
                </ul>
                <div id={styles.errorMessage}>
                    {errorMessage}
                </div>
            </span>
        </div>
    )
}

export default ShipSearch