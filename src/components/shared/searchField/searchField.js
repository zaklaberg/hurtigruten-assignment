import React, { useState } from 'react'
import styles from './searchField.module.css'

const ENTER_KEY = 13

/**
 * @param props.label: Label for search field.
 * @param props.placeholder: Placeholder for search text field.
 * @param props.onSearchClear: Function to be called when text field turns empty.
 * @param props.onSearch: Function to be called when a user executes a search.
 * @param props.searchDelay: If set, onSearch will be called [searchDelay] ms after user has stopped typing.
 */
const SearchField = props => {
    // Whether the search field is active or not. Triggered by its children(we keep a border on the parent)
    const [isActive, setIsActive] = useState(false)
    const [searchText, setSearchText] = useState('')
    const [searchTimerId, setSearchTimerId] = useState(null)

    // Used for debouncing searches to not murder the server
    // Guarantees that [onSearch] is not called more than once every [searchDelay] ms

    const isSearching = () => searchText.length > 0

    const submitSearch = e => {
        if (e.which === ENTER_KEY) {
            props.onSearch(searchText)
            return 
        }
        
        // If a change occurs and it's a user deleting back to an empty search field
        // It's not clear what to do when autosearching. Should it be considered 
        // a search? Or should it be considered a 'clearing' action?
        // I will leave it as being considered a search, but we could put a flag to check
        // if the field is empty and call clearSearch instead.
        if (props.searchDelay !== undefined) {
            // Kill any ongoing searches(that haven't reached server yet)
            if (searchTimerId) {
                clearTimeout(searchTimerId)
                setSearchTimerId(null)
            }

            // If delay is zero, no need for a timeout
            if (props.searchDelay === 0) {
                props.onSearch(searchText)
                return
            }

            // Add a search to the future.
            setSearchTimerId(setTimeout(() => {
                props.onSearch(searchText)
            }, props.searchDelay))
        }
    }

    const clearSearch = () => {
        if (searchTimerId) {
            clearTimeout(searchTimerId)
            setSearchTimerId(null)
        }
        setSearchText('')
        props.onSearchClear()
    }

    return (
        <div className={styles.searchFieldOuterContainer}>
            <label className={styles.label} htmlFor={styles.searchField}>{props.label}</label>
            <div className={`${styles.searchFieldInnerContainer} ${isActive ? styles.isActive : ''}`}>
                <input
                    id={styles.searchField}
                    type="text"
                    value={searchText}
                    placeholder={props.placeholder || 'Search'}
                    title="Type here to search"
                    onChange={e => setSearchText(e.target.value)}
                    onKeyUp={submitSearch}
                    onFocus={() => setIsActive(true)}
                    onBlur={() => setIsActive(false)}
                />
                
                { 
                /*
                    I was unsure of whether the magnifying glass "button" should have
                    any functionality, since it's being replaced in Exercise 2. Looking
                    at the way google do it, I assumed that it was not to have functionality,
                    searches being executed either with enter or automatically.
                */
                }
                <button
                    id={styles.clearButton}
                    className={isSearching() ? styles.isSearching : null}
                    type="button"
                    title="Search"
                    onFocus={() => setIsActive(true)}
                    onBlur={() => setIsActive(false)}
                    onClick={clearSearch}
                >
                    <span>Search</span>
                </button>
            </div>
        </div>
    )
}

export default SearchField